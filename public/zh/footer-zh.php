<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-footer__inner clear">
            
            <div class="site-footer-nav">
              <dl class="site-footer-nav__block -first-block">
                <dt class="site-footer-nav__title js-sptoggle">
                  <a class="site-footer-list__h-link" href="https://taiwantranslation.com/zh/translation">筆譯服務</a>
                </dt>
                <dt class="site-footer-nav__title js-sptoggle">
                  <a class="site-footer-list__h-link" href="https://taiwantranslation.com/zh/field">筆譯服務領域</a>
                </dt>
                <dt class="site-footer-nav__title js-sptoggle">
                  <a class="site-footer-list__h-link" href="https://taiwantranslation.com/zh/membership">會員專案</a>
                </dt>
                <dt class="site-footer-nav__title js-sptoggle">
                  <a class="site-footer-list__h-link" href="https://taiwantranslation.com/zh/ai">AI翻譯</a>
                </dt>
                <dt class="site-footer-nav__title js-sptoggle">
                  <a class="site-footer-list__h-link" href="https://taiwantranslation.com/zh/trial">試譯服務</a>
                </dt>
                <dt class="site-footer-nav__title js-sptoggle">
                  <a class="site-footer-list__h-link js-hashlink" href="https://taiwantranslation.com/zh/member#tab3">品質管理流程</a>
                </dt>
              </dl>

              <dl class="site-footer-nav__block -second-block">
                <dt class="site-footer-nav__title js-sptoggle">
                  <div class="site-footer-list__h-link">遊戲本地化</div>
                </dt>
                <dd class="site-footer-nav__content">
                  <ul class="site-footer-list">
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/game_localization">遊戲本地化</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/game_localization/language">語言及費用</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/game_localization/portfolio">本地化成果</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/game_localization/knowhow">高品質的理由</a>
                    </li>
                  </ul>
                </dd>
                <dt class="site-footer-nav__title js-sptoggle">
                  <div class="site-footer-list__h-link">口譯服務</div>
                </dt>
                <dd class="site-footer-nav__content">
                  <ul class="site-footer-list">
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link js-hashlink" href="https://taiwantranslation.com/zh/interpretation#price">口譯服務</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/interpretation/interpreter">口譯人員</a>
                    </li>
                  </ul>
                </dd>
              </dl>
              <dl class="site-footer-nav__block -third-block">
                <dt class="site-footer-nav__title js-sptoggle">
                  <div class="site-footer-list__h-link">關於我們</div>
                </dt>
                <dd class="site-footer-nav__content">
                  <ul class="site-footer-list">
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/about">公司簡介</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link js-hashlink" href="https://taiwantranslation.com/zh/about#tab2">總經理致詞</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/clients">我們的客戶</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/member">我們的譯者</a>
                    </li>
                  </ul>
                </dd>
                <dt class="site-footer-nav__title js-sptoggle">
                  <div class="site-footer-list__h-link">人才招募</div>
                </dt>
                <dd class="site-footer-nav__content">
                  <ul class="site-footer-list">
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link js-hashlink" href="https://taiwantranslation.com/zh/recruit">合作譯者</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link js-hashlink" href="https://taiwantranslation.com/zh/recruit#tab2">正職員工</a>
                    </li>
                  </ul>
                </dd>
              </dl>
              <dl class="site-footer-nav__block -fourth-block">
                <dt class="site-footer-nav__title">
                  <a class="site-footer-list__h-link" href="https://taiwantranslation.com/zh/e-learning">線上翻譯課程</a>
                </dt>
                <dt class="site-footer-nav__title">
                  <a class="site-footer-list__h-link" href="https://taiwantranslation.com/zh/privacy">隱私權政策</a>
                </dt>
                <dt class="site-footer-nav__title js-sptoggle">
                  <div class="site-footer-list__h-link">線上估價</div>
                </dt>
                <dd class="site-footer-nav__content">
                  <ul class="site-footer-list">
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/translation_quote">筆譯服務</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/interpretation_quote">口譯服務</a>
                    </li>
                    <li class="site-footer-list__item">
                      <a class="site-footer-list__link" href="https://taiwantranslation.com/zh/inquiry">其他洽詢</a>
                    </li>
                  </ul>
                </dd>
              </dl>
            </div><!-- .footer-nav -->
            
            
            <div class="site-footer-info">
                <div class="site-footer-info__logo">
                    <img class="site-footer-info__image" src="<?php bloginfo('template_directory'); ?>/img/logo_mts-footer.png" alt="米耶翻譯股份有限公司 Mie Translation Services">
                </div>
                <div class="site-footer-info__address">
                    地址：10563台北市松山區光復南路65號2樓
                </div>
                <div class="site-footer-info__tel">
                    <span class="disp-large">TEL：02-2765-2925</span>
                    <span class="disp-small">TEL：<a href="tel:02-2765-2925">02-2765-2925</a></span>
                </div>
                <ul class="site-footer-sns clear">
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://www.facebook.com/MieManagementService/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_facebook.png" alt="facebook"></a>
                    </li>
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://www.instagram.com/mietranslation/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_instagram.png" alt="中国語の翻訳会社Mie Translation ServicesのInstagram"></a>
                    </li>          
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://zw.linkedin.com/company/taiwantranslation" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_linkedin.png" alt="linkdin"></a>
                    </li>
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://www.youtube.com/channel/UCo1oQflk1GkJaiz0-SprQlg/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_youtube.png" alt="youtube"></a>
                    </li>
                </ul>
            </div><!-- .footer-info -->
            
            
            <p class="site-footer-copy">
                Mie Translation Services Co.,Ltd. All rights reserved.
            </p>
			
		</div><!-- .site-footer__inner -->
	</footer><!-- #colophon -->
</div><!-- #page -->



<?php wp_footer(); ?>

</body>
</html>