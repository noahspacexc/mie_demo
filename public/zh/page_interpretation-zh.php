<?php
/**
 * Template Name:中国語カラムなし（Z6:通訳サービス用）
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage MTS
 * @since MTS 1.0
 */
 
get_header("zh"); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            
            
            <!--div class="c-sub-hero">
                <img class="c-sub-hero__image disp-large" src="/wp-content/themes/mts/img/zh/hero_lower_zh.png" alt="日文筆譯、英文筆譯、日文口譯">
                <img class="c-sub-hero__image disp-small" src="/wp-content/themes/mts/img/zh/hero_lower_sp_zh.png" alt="日文筆譯、英文筆譯、日文口譯">
            </div-->
            
            <div class="entry-content">
                <!--h1 class="c-page-heading-zh">口譯服務</h1-->

                <div class="c-tab">

                    <div class="c-tab-nav-wrap">
                        <ul class="c-tab-nav -tab4">
                            <li id="tab1" class="c-tab-nav__item -active">口譯費用與服務地點</li>
                            <li id="tab2" class="c-tab-nav__item ">口譯人員</li>
                            <li id="tab3" class="c-tab-nav__item ">委託方式與付款方式</li>
                            <!--li id="tab3" class="c-tab-nav__item ">展覽會資訊</li-->
                        </ul>
                    </div>


                    <div class="c-tab-body">

                        <!-- ▼口譯費用與服務地點 -->
                        <div class="c-tab-body__item -active">
                            <?php echo do_shortcode("[insert page='interpretation-tab1' display='content']"); ?>
                        </div><!-- /c-tab-body__item -->

                        <!-- ▼口譯人員 -->
                        <div class="c-tab-body__item">
                            <?php echo do_shortcode("[insert page='interpretation-tab2' display='content']"); ?>
                        </div><!-- /c-tab-body__item -->

                        <!-- ▼委託方式與付款方式 -->
                        <div class="c-tab-body__item">
                            <?php echo do_shortcode("[insert page='interpretation-tab3' display='content']"); ?>
                        </div><!-- /c-tab-body__item -->

                    </div><!-- /c-tab-body -->


                </div><!-- /c-tab -->
                
            </div><!-- /entry-content -->

            
            

		</main> 
        <!-- #main -->
            
	</div><!-- #primary -->

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-zh-TW.min.js"></script>
<!-- <script type='text/javascript' src='/wp-content/themes/mts/js/jquery.matchHeight.js'></script> -->
<script type="text/javascript">
    jQuery(function ($) {
        
           //▼共通タブ
            //クリックしたときのファンクションをまとめて指定
            $('.c-tab-nav .c-tab-nav__item').click(function() {
                //.index()を使いクリックされたタブが何番目かを調べ、
                // indexという変数に代入します。
                var index = $('.c-tab-nav .c-tab-nav__item').index(this);

                //コンテンツを一度すべて非表示にし、
                $('.c-tab-body__item').css('display','none');

                //クリックされたタブと同じ順番のコンテンツを表示します。
                $('.c-tab-body__item').eq(index).fadeIn();

                //タブについているクラスselectを消し、
                $('.c-tab-nav .c-tab-nav__item').removeClass('-active');

                //クリックされたタブのみにクラスselectをつけます。
                $(this).addClass('-active')
            });


        //▼別ページからのタブリンク
         //location.hashで#以下を取得 変数hashに格納
         var hash = location.hash;
         //hashの中に#tab～が存在するか調べる。
         hash = (hash.match(/^#tab\d+$/) || [])[0];
         //hashに要素が存在する場合、hashで取得した文字列（#tab2,#tab3等）から#より後を取得(tab2,tab3) 
         if($(hash).length){
             var tabname = hash.slice(1) ;
         } else{
             // 要素が存在しなければtabnameにtab1を代入する
             var tabname = "tab1";
         }
         //コンテンツを一度すべて非表示にし、
         $('.c-tab-body__item').css('display','none');
         //一度タブについているクラス-activeを消し、
         $('.c-tab-nav .c-tab-nav__item').removeClass('-active');
         var tabno = $('.c-tab-nav .c-tab-nav__item#' + tabname).index();
         //クリックされたタブと同じ順番のコンテンツを表示します。
         $('.c-tab-body__item').eq(tabno).fadeIn();
         //クリックされたタブのみにクラスselectをつけます。
         $('.c-tab-nav .c-tab-nav__item').eq(tabno).addClass('-active');
        
        //▼同一ページからのタブリンク
        //location.hashで#以下を取得 変数hashに格納
        $('.js-hashlink').click(function() {
            setTimeout(function(){   
                var hash2 = location.hash;
                //hashの中に#tab～が存在するか調べる。
                hash2 = (hash2.match(/^#tab\d+$/) || [])[0];
                //hashに要素が存在する場合、hashで取得した文字列（#tab2,#tab3等）から#より後を取得(tab2,tab3) 
                if($(hash2).length){
                    var tabname = hash2.slice(1) ;
                } else{
                    // 要素が存在しなければtabnameにtab1を代入する
                    var tabname = "tab1";
                }
                //コンテンツを一度すべて非表示にし、
                $('.c-tab-body__item').css('display','none');
                //一度タブについているクラス-activeを消し、
                $('.c-tab-nav .c-tab-nav__item').removeClass('-active');
                var tabno = $('.c-tab-nav .c-tab-nav__item#' + tabname).index();
                //クリックされたタブと同じ順番のコンテンツを表示します。
                $('.c-tab-body__item').eq(tabno).fadeIn();
                //クリックされたタブのみにクラスselectをつけます。
                $('.c-tab-nav .c-tab-nav__item').eq(tabno).addClass('-active');

                var urlHash = location.hash;
                //ハッシュ値があればページ内スクロール
                if(urlHash) {
                    //スクロールを0に戻しておく
                    $('body,html').stop().scrollTop(240);
                    setTimeout(function () {
                        //ロード時の処理を待ち、時間差でスクロール実行
                        scrollToAnker(urlHash) ;
                    }, 100);
                }
            },10);
        });


        // ▼共通タブ（横スクロール）
        var array = [];
        for(var i = 0; i < $(".c-tab-nav .c-tab-nav__item").length; i++){
            array.push($(".c-tab-nav .c-tab-nav__item").eq(i).outerWidth());
        }
        var childElementWidth = 0;
        for(var j = 0; j < array.length; j++){
            childElementWidth += array[j];
        }
        var navWrap = $(".c-tab").width();
        if (childElementWidth > navWrap ) {
            $(".c-tab-nav").width(childElementWidth + j*4);
        }


        //▼ハッシュ付きのページスクロール
        //URLのハッシュ値を取得
        var urlHash = location.hash;
        //ハッシュ値があればページ内スクロール
        if(urlHash) {
            //スクロールを0に戻しておく
            $('body,html').stop().scrollTop(0);
            setTimeout(function () {
              //ロード時の処理を待ち、時間差でスクロール実行
              scrollToAnker(urlHash) ;
            }, 100);
        }
        //通常のクリック時
        $('a[href^="#"]').click(function() {
            //ページ内リンク先を取得
            var href= $(this).attr("href");
            //リンク先が#か空だったらhtmlに
            var hash = href == "#" || href == "" ? 'html' : href;
            //スクロール実行
            scrollToAnker(hash);
            return false;
        });
        // 関数：スムーススクロール
        // 指定したアンカー(#ID)へアニメーションでスクロール
        function scrollToAnker(hash) {
            var target = $(hash);
            var position = target.offset().top;
            console.log('position');
            $('body,html').stop().animate({scrollTop:position-90}, 500);
        }
        
        //Z6 展示会情報のセレクトボックス年切り替え
       //  $('.exhibition-zh-table-wrap.-active' ).fadeIn();
        $('.exhibition-zh-change__select').change(function () {
            var val = $('.exhibition-zh-change__select option:selected').val();
            if (val == 'exhibition-zh-table-wrap') return;
            $('.exhibition-zh-table-wrap').hide();
            $('.exhibition-zh-table-wrap#' + val ).fadeIn();
            $('.exhibition-zh-table-wrap#').addClass('.-active');
        });
        
        //現在の西暦にチェックを入れる
        var d = new Date();
        var year = d.getFullYear();
        var exhibitionYear =  "exhibition"+ year;
        $(".exhibition-change__select").val(exhibitionYear);
        

    });
</script>


<?php
//get_sidebar();
get_footer("zh");

