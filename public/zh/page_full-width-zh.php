<?php
/**
 * Template Name:中国語カラムなし
 *
 * This is the template that displays all pages by default.
 * Please note that this is the WordPress construct of pages and that
 * other "pages" on your WordPress site will use a different template.
 *
 * @package WordPress
 * @subpackage MTS
 * @since MTS 1.0
 */
 
get_header("zh"); ?>

	<div id="primary" class="content-area">
		<main id="main" class="site-main">
            
            
            <!--div class="c-sub-hero">
                <img class="c-sub-hero__image disp-large" src="/wp-content/themes/mts/img/zh/hero_lower_zh.png" alt="日文筆譯、英文筆譯、日文口譯">
                <img class="c-sub-hero__image disp-small" src="/wp-content/themes/mts/img/zh/hero_lower_sp_zh.png" alt="日文筆譯、英文筆譯、日文口譯">
            </div-->

            <?php
            while ( have_posts() ) :
                the_post();

                get_template_part( 'template-parts/content', 'page' );

                // If comments are open or we have at least one comment, load up the comment template.
                if ( comments_open() || get_comments_number() ) :
                    comments_template();
                endif;

            endwhile; // End of the loop.
            ?>
            

		</main> 
        <!-- #main -->
            
	</div><!-- #primary -->

<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1/i18n/jquery.ui.datepicker-zh-TW.min.js"></script>
<script type='text/javascript' src='/wp-content/themes/mts/js/jquery.matchHeight.js'></script>
<script type="text/javascript">
    jQuery(function ($) {
        
            //▼高さを揃える
            // J3翻訳分野のブロック高さを揃える
            $('.honyaku-bunya-list__item').matchHeight();
            $('.payment-zh-list__heading').matchHeight();


           //▼共通タブ
            //クリックしたときのファンクションをまとめて指定
            $('.c-tab-nav .c-tab-nav__item').click(function() {
                //.index()を使いクリックされたタブが何番目かを調べ、
                // indexという変数に代入します。
                var index = $('.c-tab-nav .c-tab-nav__item').index(this);

                //コンテンツを一度すべて非表示にし、
                $('.c-tab-body__item').css('display','none');

                //クリックされたタブと同じ順番のコンテンツを表示します。
                $('.c-tab-body__item').eq(index).fadeIn();

                //タブについているクラスselectを消し、
                $('.c-tab-nav .c-tab-nav__item').removeClass('-active');

                //クリックされたタブのみにクラスselectをつけます。
                $(this).addClass('-active')
            });


        //▼別ページからのタブリンク
         //location.hashで#以下を取得 変数hashに格納
         var hash = location.hash;
         //hashの中に#tab～が存在するか調べる。
         hash = (hash.match(/^#tab\d+$/) || [])[0];
         //hashに要素が存在する場合、hashで取得した文字列（#tab2,#tab3等）から#より後を取得(tab2,tab3) 
         if($(hash).length){
             var tabname = hash.slice(1) ;
         } else{
             // 要素が存在しなければtabnameにtab1を代入する
             var tabname = "tab1";
         }
         //コンテンツを一度すべて非表示にし、
         $('.c-tab-body__item').css('display','none');
         //一度タブについているクラス-activeを消し、
         $('.c-tab-nav .c-tab-nav__item').removeClass('-active');
         var tabno = $('.c-tab-nav .c-tab-nav__item#' + tabname).index();
         //クリックされたタブと同じ順番のコンテンツを表示します。
         $('.c-tab-body__item').eq(tabno).fadeIn();
         //クリックされたタブのみにクラスselectをつけます。
         $('.c-tab-nav .c-tab-nav__item').eq(tabno).addClass('-active');
        
        
        //▼同一ページからのタブリンク
        //location.hashで#以下を取得 変数hashに格納
        $('.js-hashlink').click(function() {
            setTimeout(function(){   
                var hash2 = location.hash;
                //hashの中に#tab～が存在するか調べる。
                hash2 = (hash2.match(/^#tab\d+$/) || [])[0];
                //hashに要素が存在する場合、hashで取得した文字列（#tab2,#tab3等）から#より後を取得(tab2,tab3) 
                if($(hash2).length){
                    var tabname = hash2.slice(1) ;
                } else{
                    // 要素が存在しなければtabnameにtab1を代入する
                    var tabname = "tab1";
                }
                //コンテンツを一度すべて非表示にし、
                $('.c-tab-body__item').css('display','none');
                //一度タブについているクラス-activeを消し、
                $('.c-tab-nav .c-tab-nav__item').removeClass('-active');
                var tabno = $('.c-tab-nav .c-tab-nav__item#' + tabname).index();
                //クリックされたタブと同じ順番のコンテンツを表示します。
                $('.c-tab-body__item').eq(tabno).fadeIn();
                //クリックされたタブのみにクラスselectをつけます。
                $('.c-tab-nav .c-tab-nav__item').eq(tabno).addClass('-active');

                var urlHash = location.hash;
                //ハッシュ値があればページ内スクロール
                if(urlHash) {
                    //スクロールを0に戻しておく
                    $('body,html').stop().scrollTop(240);
                    setTimeout(function () {
                        //ロード時の処理を待ち、時間差でスクロール実行
                        scrollToAnker(urlHash) ;
                    }, 100);
                }
            },10);
        });
        
        


        // ▼共通タブ（横スクロール）
        $(window).on('load resize', function(){

            var array = [];
            for(var i = 0; i < $(".c-tab-nav .c-tab-nav__item").length; i++){
                array.push($(".c-tab-nav .c-tab-nav__item").eq(i).outerWidth());
            }
            var childElementWidth = 0;
            for(var j = 0; j < array.length; j++){
                childElementWidth += array[j];
            }

            childElementWidth +=  j* 4;   //マージンの4px分
            
            var navWrap = $(".c-tab").width();
            if (childElementWidth > navWrap ) {
                $(".c-tab-nav").width(childElementWidth);
            } else {
                $(".c-tab-nav").css({'width':'auto'});
            }
            
            if ($(window).width() < 361) {
                if (childElementWidth  > navWrap ) {
                    $(".c-tab-nav.-tab3").width(childElementWidth + j* 4);
                    $(".c-tab-nav.-tab2").width(childElementWidth + j* 31);
                } else {
                    $(".c-tab-nav").css({'width':'auto'});
                }
            }

        });


        //▼ハッシュ付きのページスクロール
        //URLのハッシュ値を取得
        var urlHash = location.hash;
        //ハッシュ値があればページ内スクロール
        if(urlHash) {
            //スクロールを0に戻しておく
            $('body,html').stop().scrollTop(0);
            setTimeout(function () {
              //ロード時の処理を待ち、時間差でスクロール実行
              scrollToAnker(urlHash) ;
            }, 100);
        }
        //通常のクリック時
        $('a[href^="#"]').click(function() {
            //ページ内リンク先を取得
            var href= $(this).attr("href");
            //リンク先が#か空だったらhtmlに
            var hash = href == "#" || href == "" ? 'html' : href;
            //スクロール実行
            scrollToAnker(hash);
            return false;
        });
        
        
        // 関数：スムーススクロール
        // 指定したアンカー(#ID)へアニメーションでスクロール
        function scrollToAnker(hash) {
            var target = $(hash);
            var position2 = target.offset().top;
            //console.log('position2');
            $('body,html').stop().animate({scrollTop:position2-100}, 500);
            
        }

        

        //Z6 展示会情報のセレクトボックス年切り替え
       //  $('.exhibition-zh-table-wrap.-active' ).fadeIn();
        $('.exhibition-zh-change__select').change(function () {
            var val = $('.exhibition-zh-change__select option:selected').val();
            if (val == 'exhibition-zh-table-wrap') return;
            $('.exhibition-zh-table-wrap').hide();
            $('.exhibition-zh-table-wrap#' + val ).fadeIn();
            $('.exhibition-zh-table-wrap#').addClass('.-active');
        });
        
        
        // お問い合わせフォームのエラー
        /*   var offsetPosition =  $('form.wpcf7-form');
        var position3 = offsetPosition.offset().top; */
        $('.wpcf7').on('wpcf7:invalid', function() {
            /*
            $('html, body').animate({
                scrollTop: position3 -100
            }, 700);
            */
            $(window).scrollTop(330);
        });

        // お問い合わせフォーム：ラジオ（その他） 
        $('.c-form-inupt__disabled-text').attr('disabled','disabled'); 

        $( 'input[name="interpretation__place"]:radio' ).change( function() {
            var radioval = $(this).val();
            if(radioval == "其他"){
                $('.c-form-inupt__disabled-text').removeAttr('disabled');
            }else{
                $('.c-form-inupt__disabled-text').attr('disabled','disabled'); 
            }
        });    
        
        
        $('.c-form-inupt-detail__disabled-text').attr('disabled','disabled'); 
        $( 'input[name="interpretation__detail"]:radio' ).change( function() {
            var radioval = $(this).val();
            if(radioval == "其他"){
                $('.c-form-inupt-detail__disabled-text').removeAttr('disabled');
            }else{
                $('.c-form-inupt-detail__disabled-text').attr('disabled','disabled'); 
            }
        });    

        
        // お問い合わせフォーム：チェックボックス（その他）
        $('.c-form-inupt__disabled-text').attr('disabled','disabled'); 
        $('.c-form-inupt__checkbox .last input').click(function() {
            if ($(this).prop('checked') == false) {
                $('.c-form-inupt__disabled-text').attr('disabled', 'disabled');
                $(".c-form-inupt__disabled-text").val("");
            } else {
                $('.c-form-inupt__disabled-text').removeAttr('disabled');
            }
        });


        // お問い合わせフォーム：セレクトボックス（その他）
        $('.c-form-inupt__disabled-text2').hide(); 
        $(".interpretation__detail select").change(function(){
            var name = $('.interpretation__detail select').val()
            if(name == '其他（請填寫具體內容）'){
                $('.c-form-inupt__disabled-text2').show(); 
            } else {
                $('.c-form-inupt__disabled-text2').hide(); 
            }
        });


        // お問い合わせフォーム：カレンダーピッカー
        $.datepicker.setDefaults($.datepicker.regional["zh-TW"]);
        $(".js-calendar").datepicker();



    });
</script>


<?php
//get_sidebar();
get_footer("zh");

