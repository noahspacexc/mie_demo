<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
<meta charset="<?php bloginfo( 'charset' ); ?>">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="format-detection" content="telephone=no">
<link href="https://fonts.googleapis.com/css?family=Roboto&display=swap" rel="stylesheet">
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<link rel="shortcut icon" href="/wp-content/themes/mts/favicon.ico">
<script src="https://code.jquery.com/jquery-3.5.1.js"></script>
<!-- 管理画面ログイン時の上部メニュー分の削除 -->
<style>
html {margin-top: 0 !important;}
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src="https://www.googletagmanager.com/gtag/js?id=UA-5017112-4"></script>
<script>
window.dataLayer = window.dataLayer || [];
function gtag(){dataLayer.push(arguments);}
gtag('js', new Date());
gtag('config', 'UA-5017112-4');
</script>
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MS42KD');</script>
<!-- End Google Tag Manager -->
<?php wp_head(); ?>    
</head>

<body <?php body_class(); ?>>
   
    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS42KD"
                      height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->
      
   
    <div id="page" class="site site-ja">
	<a class="skip-link screen-reader-text" href="#content"><?php esc_html_e( 'Skip to content', 'mts' ); ?></a>

	<header id="masthead" class="site-header">
		
        <!-- ▼サイトロゴ -->
        <div class="site-branding">
          <a href="https://taiwantranslation.com/" class="custom-logo-link" rel="home"><img src="/wp-content/themes/mts/img/logo.png" class="custom-logo" alt="Mie Translation Services"></a>
        <?php
            //the_custom_logo();
            if ( is_front_page() && is_home() ) :
        ?>
        <h1 class="site-title"><a href="<?php echo esc_url( home_url( '/' ) ); ?>" rel="home"><?php bloginfo( 'name' ); ?></a></h1>
        <?php
            else :
        ?>
				<!-- <span class="site-title disp-large">中国語・台湾語を専門とする翻訳会社です</span> -->
				<?php
          endif;
          $mts_description = get_bloginfo( 'description', 'display' );
          if ( $mts_description || is_customize_preview() ) :
				?>
				<!-- <p class="site-description"><?php echo $mts_description; /* WPCS: xss ok. */ ?></p> -->
			<?php endif; ?>
		</div><!-- .site-branding -->
        
        
        <!-- ▼メインナビゲーション -->
        <div class="c-overlay"></div>
        <div class="menu-toggle"><span></span></div>
        <nav id="site-navigation" class="main-navigation">
            <div class="main-navigation__inner">
                <!-- スマホ用メニューボタン -->
                <!-- <button class="menu-toggle" aria-controls="primary-menu" aria-expanded="false"><span></span></button> -->

                <!-- メニューリスト -->
                <?php
                wp_nav_menu( array(
                    'theme_location' => 'menu-1',
                    'menu_id'        => 'primary-menu',
                    'menu_class'        => 'nav-menu',
                ) );
                ?>

                <div class="site-language-sp">
                    <?php if ( function_exists( 'the_msls' ) ) the_msls(); ?>
                </div>

                <div class="menubnr-sp">
                    <ul class="menubnr-sp-list">
                        <li class="menubnr-sp-list__item">
                            <a class="menubnr-sp-list__link" href="/recruit">
                                <img class="menubnr-sp-list__image" src="<?php bloginfo('template_directory'); ?>/img/bnr_freelance_sp.png" alt="フリーランス募集中">
                            </a>
                        </li>
                        <!--
                        <li class="menubnr-sp-list__item">
                            <a class="menubnr-sp-list__link" href="/recruit">
                                <img class="menubnr-sp-list__image" src="<?php bloginfo('template_directory'); ?>/img/bnr_freelance_sp.png" alt="フリーランス募集中">
                            </a>
                        </li>
                        -->
                    </ul>
                </div>
                
            </div><!-- .main-navigation__inner -->
            
		</nav><!-- #site-navigation -->
        
        
        <!-- 言語切替スイッチャー -->
        <!-- <div class="site-language">
            <?php if ( function_exists( 'the_msls' ) ) the_msls(); ?>
        </div> -->
        <div class="site-language">
          <div class="site-language__btn">
            <div class="site-language__link" href="">日本語</div>
            <ul class="site-language-list">
              <li class="site-language-list__item">
                <a href="https://en.taiwantranslation.com/" title="English">English</a>
              </li>
              <li class="site-language-list__item">
                <a href="https://taiwantranslation.com/zh/" title="中文">中文</a>
              </li>
            </ul>
          </div>
        </div>

        
        <!-- ▼お問い合わせナビ -->
        <nav id="site-navigation-contact" class="contact-nav">
            <ul class="contact-nav-list">
                <li class="contact-nav-list__item contact-nav-list__item--contact">
                  <div class="contact-nav-list__link">
                      <img class="contact-nav-list__image disp-large-ib" src="<?php bloginfo('template_directory'); ?>/img/text_head-contact.png" alt="お問い合わせ">
                      <img class="contact-nav-list__image disp-small-ib" src="<?php bloginfo('template_directory'); ?>/img/text_head-contact_sp.png" alt="お問い合わせ">
                  </div>                   
                  <ul class="contact-nav-sublist">
                    <li class="contact-nav-sublist__item">
                      <a class="contact-nav-sublist__link" href="/translation_quote">翻訳のお問い合わせ</a>
                    </li>
                    <li class="contact-nav-sublist__item">
                      <a class="contact-nav-sublist__link" href="/interpretation_quote">通訳のお見積り</a>
                    </li>
                  </ul>
                </li>
                <!-- <li class="contact-nav-list__item contact-nav-list__item--freelance">
                    <a class="contact-nav-list__link" href="/recruit"><img class="contact-nav-list__image" src="<?php bloginfo('template_directory'); ?>/img/text_head-freelance.png" alt="フリーランス翻訳者募集中"></a>
                </li> -->
            </ul>
        </nav>
        
        <script type="text/javascript">
          
          jQuery(function ($) {
              
              // メニュードロップダウンメニュー
              if ($(window).width() > 1000) {
                $('.main-navigation ul.nav-menu > li, .main-navigation ul.nav-menu .menu-item-has-children').hover(
                  function() {
                    //カーソルが重なった時
                    $(this).children('.sub-menu').addClass('active');
                  }, function() {
                    //カーソルが離れた時
                   $(this).children('.sub-menu').removeClass('active');
                  }
                );
              }
              
              // メニュードロップダウンメニュー
              if ($(window).width() < 1000) {
              $(function(){
                $(".main-navigation ul.nav-menu > li a").on("click", function(e) {
                  if(e.currentTarget.parentElement.id === "menu-item-74") {
                    return
                  }

                  $(this).next().slideToggle();
                  $(this).toggleClass("active");
                });
              });
            }
              
              // 言語切替用のドロップダウンメニュー
              $('.site-language__btn').hover(
                function() {
                  //カーソルが重なった時
                  $(this).children('.site-language-list').addClass('active');
                }, function() {
                  //カーソルが離れた時
                 $(this).children('.site-language-list').removeClass('active');
                }
              );
              
              // お問い合わせ用のドロップダウンメニュー
              $('.contact-nav-list__item').hover(
                function() {
                  //カーソルが重なった時
                  $(this).children('.contact-nav-sublist').addClass('active');
                }, function() {
                  //カーソルが離れた時
                 $(this).children('.contact-nav-sublist').removeClass('active');
                }
              );
              
              // スマホ用メニュー
              $('.menu-toggle').click(function(){
                if($(this).hasClass("toggled")){ 
                  $(this).removeClass("toggled");
                  $('#site-navigation').removeClass("toggled");    
                  $('.c-overlay').hide();
                }else{
                  $(this).addClass("toggled");
                  $('#site-navigation').addClass("toggled");
                  $('.c-overlay').show(); 
                }
              });
              // オーバーレイから閉じる
              $('.c-overlay').click(function(){
                if($('.menu-toggle').hasClass("toggled")){ 
                  $('.menu-toggle').removeClass("toggled");
                  $('#site-navigation').removeClass("toggled");    
                  $('.c-overlay').hide();  
                }else{
                  $('.menu-toggle').addClass("toggled");
                  $('#site-navigation').addClass("toggled");
                }
              });
              // スマホ用フッターメニュー
              if ($(window).width() < 1000) {
                $(function(){
                  $('.js-sptoggle').on("click", function() {
                    $(this).next().slideToggle();
                    $(this).toggleClass("active")
                  });
                });
              }
              
              // 言語切り替えスイッチャーの位置調整（Englishを最後に）
              $('.site-language-list__item:last-child').after($('.site-language-list__item:first-child'));
              $('.site-language-list__item:last-child').hide();
              

              
              
          });
        </script>
        
        
	</header><!-- #masthead -->

	<div id="content" class="site-content">
