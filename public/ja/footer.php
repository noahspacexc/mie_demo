<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package MTS
 */

?>

	</div><!-- #content -->

	<footer id="colophon" class="site-footer">
		<div class="site-footer__inner clear">
            
            <div class="site-footer-nav">
                <div class="site-footer-nav__block ">
                    <div class="site-footer-nav__title js-sptoggle">
                        <div class="site-footer-list__h-link">翻訳サービス</div>
                    </div>
                    <div class="site-footer-nav__content">
                        <ul class="site-footer-list">
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/language">翻訳言語</a>
                            </li>
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/field">翻訳分野</a>
                            </li>
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/price">翻訳料金</a>
                            </li>
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/trial">無料トライアル翻訳</a>
                            </li>
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/ai">AI翻訳サービス</a>
                            </li>
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/transcription">音声データのテキスト起こし</a>
                            </li>
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/quality_evaluation">中国語テキスト診断</a>
                            </li>
                        </ul>
                    </div>
                    <div class="site-footer-nav__title">
                        <a class="site-footer-list__h-link" href="https://taiwantranslation.com/taiwanese">台湾語翻訳のホントのところ</a>
                    </div>
                </div>
                
                
                
                <div class="site-footer-nav__block">
                    <div class="site-footer-nav__title">
                        <div class="site-footer-list__h-link js-sptoggle">ゲームローカライズ</div>
                        <div class="site-footer-nav__content">
                            <ul class="site-footer-list">
                                <li class="site-footer-list__item">
                                    <a class="site-footer-list__link" href="https://taiwantranslation.com/game_localization">ゲームローカライズ</a>
                                </li>
                                <li class="site-footer-list__item">
                                    <a class="site-footer-list__link" href="https://taiwantranslation.com/game_localization/knowhow">美しいインゲームテキスト</a>
                                </li>
                                <li class="site-footer-list__item">
                                    <a class="site-footer-list__link" href="https://taiwantranslation.com/game_localization/lqa"> LQAサービス</a>
                                </li>
                                <li class="site-footer-list__item">
                                    <a class="site-footer-list__link" href="https://taiwantranslation.com/game_localization/portfolio">ゲーム翻訳実績</a>
                                </li>
                                <li class="site-footer-list__item">
                                    <a class="site-footer-list__link" href="https://taiwantranslation.com/game_localization/language">ゲーム翻訳料金</a>
                                </li>
                                <li class="site-footer-list__item">
                                    <a class="site-footer-list__link" href="https://taiwantranslation.com/game_localization/translatortraining">ゲーム翻訳者の育成</a>
                                </li>
                            </ul>
                        </div>
                    </div>
                    <div class="site-footer-nav__title">
                        <a class="site-footer-list__h-link" href="https://taiwantranslation.com/interpretation">通訳サービス</a>
                    </div>
                </div>
                

                
                <div class="site-footer-nav__block -third-block">
                    <div class="site-footer-nav__title js-sptoggle">
                        <div class="site-footer-list__h-link">お問い合わせ</div>
                    </div>
                    <div class="site-footer-nav__content">
                        <ul class="site-footer-list">
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/translation_quote">翻訳のお問合わせ</a>
                            </li>
                            <li class="site-footer-list__item">
                                <a class="site-footer-list__link" href="https://taiwantranslation.com/interpretation_quote">通訳のお見積り</a>
                            </li>
                        </ul>
                    </div>
                    <div class="site-footer-nav__title">
                        <a class="site-footer-list__h-link" href="https://taiwantranslation.com/about">私たちについて</a>
                    </div>
                    <div class="site-footer-nav__title">
                        <a class="site-footer-list__h-link" href="https://taiwantranslation.com/recruit">求人情報</a>
                    </div>
                    <div class="site-footer-nav__title">
                        <a class="site-footer-list__h-link" href="https://taiwantranslation.com/privacy">プライバシーポリシー</a>
                    </div>
                </div>
            </div><!-- .footer-nav -->
            
            
            <div class="site-footer-info">
                <div class="site-footer-info__logo">
                    <img class="site-footer-info__image" src="<?php bloginfo('template_directory'); ?>/img/logo_mts-footer.png" alt="中国語（台湾語）と日本語の翻訳会社｜米耶翻譯股份有限公司 Mie Translation Services">
                </div>
                <div class="site-footer-info__address">
                    住所：10563 台湾台北市松山区光復南路65号2F
                </div>
                <div class="site-footer-info__tel">
                    <span class="disp-large">
                        電話：日本から 050-5532-2926<br>
                        台湾から 02-2765-2925
                    </span>
                    <span class="disp-small">
                        電話：日本から <a href="tel:050-5532-2926">050-5532-2926</a><br>
                        台湾から <a href="tel:02-2765-2925">02-2765-2925</a>
                    </span>
                    <img class="site-footer-info__tel-image" src="<?php bloginfo('template_directory'); ?>/img/img_footer-pop.png" alt="日本語でどうぞ">
                </div>
                <ul class="site-footer-sns clear">
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://www.facebook.com/MieManagementService/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_facebook.png" alt="中国語の翻訳会社Mie Translation Servicesのfacebook"></a>
                    </li>
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://www.instagram.com/mietranslation/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_instagram.png" alt="中国語の翻訳会社Mie Translation ServicesのInstagram"></a>
                    </li>                    
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://zw.linkedin.com/company/taiwantranslation" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_linkedin.png" alt="中国語の翻訳会社Mie Translation Servicesのlinkdin"></a>
                    </li>
                    <li class="site-footer-sns__item">
                        <a class="site-footer-sns__link" href="https://www.youtube.com/channel/UCo1oQflk1GkJaiz0-SprQlg/" target="_blank"><img src="<?php bloginfo('template_directory'); ?>/img/icon_youtube.png" alt="中国語の翻訳会社Mie Translation Servicesのyoutube"></a>
                    </li>
                </ul>
            </div><!-- .footer-info -->
            
            
            <p class="site-footer-copy">
                Mie Translation Services Co.,Ltd. All rights reserved.
            </p>
			
		</div><!-- .site-footer__inner -->
	</footer><!-- #colophon -->
</div><!-- #page -->

<script src="<?php bloginfo('template_directory'); ?>/js/main.js"></script>

<?php wp_footer(); ?>

</body>
</html>